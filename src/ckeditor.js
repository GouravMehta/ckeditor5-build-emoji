/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

// The editor creator to use.
import ClassicEditorBase from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import InlineEditorBase from '@ckeditor/ckeditor5-editor-inline/src/inlineeditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import List from '@ckeditor/ckeditor5-list/src/list';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice';
import Table from '@ckeditor/ckeditor5-table/src/table';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
import CodeBlock from '@ckeditor/ckeditor5-code-block/src/codeblock';
import Emoji from '@wwalc/ckeditor5-emoji/src/emoji';
import Mention from '@ckeditor/ckeditor5-mention/src/mention';
import TextTransformation from '@ckeditor/ckeditor5-typing/src/texttransformation';
import Base64UploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/base64uploadadapter';
import EasyImage from '@ckeditor/ckeditor5-easy-image/src/easyimage';

class ClassicEditor extends ClassicEditorBase {}
class InlineEditor extends InlineEditorBase {}
//export default class InlineEditor extends InlineEditorBase {}


const plugins = [
	Essentials,
	Autoformat,
	Bold,
	Italic,
	Strikethrough,
	Underline,
	Heading,
	List,
	Paragraph,
	PasteFromOffice,
	Mention,
	TextTransformation,
	Emoji,
	EasyImage,
	Base64UploadAdapter
];


const config = {
	toolbar: {
		items: [
			'heading',
			'|',
			'bold',
			'italic',
			'strikethrough',
			'underline',
			'bulletedList',
			'numberedList',
			'|',
			'undo',
			'redo',
			'emoji',
			'imageUpload'
		]
	},
	heading: {
		options: [
			{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
			{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
			{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
			{ model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
			{ model: 'heading4', view: 'h3', title: 'Heading 4', class: 'ck-heading_heading4' }
		]
	},
	emoji: [
		{ name: 'smile', text: '😊' },
		{ name: 'wink', text: '😉' },
		{ name: 'cool', text: '😎' },
		{ name: 'surprise', text: '😮' },
		{ name: 'confusion', text: '🤔' },
		{ name: 'crying', text: '😥' },              
		{ name: 'Grimacing Face', text: '😬'},
		{ name: 'See-No-Evil Monkey', text: '🙈'},
		{ name: 'Thumbs Up', text: '👍'},
		{ name: 'Thumbs Down', text: '👎'},
		{ name: 'flexed biceps', text: '💪'},
		{ name: 'White Heavy Check Mark', text: '✅'},
		{ name: 'Cross Mark', text: '❌'},
		{ name: 'Heavy Plus Sign', text: '➕'},
		{ name: 'Heavy Minus Sign', text: '➖'},
		{ name: 'Question Mark', text: '❓'},
		{ name: 'Exclamation Mark', text: '❗'},
		{ name: 'Light Bulb', text: '💡'},
		{ name: 'Wrench', text: '🔧'},
		{ name: 'Star', text: '⭐'},
		{ name: 'Warning', text: '⚠️'} 
	],
	table: {
		contentToolbar: [
			'tableColumn',
			'tableRow',
			'mergeTableCells'
		]
	},
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: 'de'
};


ClassicEditor.builtinPlugins = plugins;
ClassicEditor.defaultConfig = config;

const inlinePlugins = [
	Essentials,
	Autoformat,
	Bold,
	Italic,
	Strikethrough,
	Underline,
	Heading,
	List,
	Paragraph,
	PasteFromOffice,
	Table,
	TableToolbar,
	CodeBlock,
	Emoji
];


const inlineConfig = {
	toolbar: {
		viewportTopOffset: 0,
		items: [
			'heading',
			'|',
			'bold',
			'italic',
			'strikethrough',
			'underline',
			'|',
			'bulletedList',
			'numberedList',
			'|',
			'undo',
			'redo',
			'|',
			'insertTable',
			'codeBlock',
			'emoji'
		]
	},
	heading: {
		options: [
			{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
			{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
			{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
			{ model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
			{ model: 'heading4', view: 'h3', title: 'Heading 4', class: 'ck-heading_heading4' }
		]
	},
	emoji: [
		{ name: 'smile', text: '😊' },
		{ name: 'wink', text: '😉' },
		{ name: 'cool', text: '😎' },
		{ name: 'surprise', text: '😮' },
		{ name: 'confusion', text: '🤔' },
		{ name: 'crying', text: '😥' },              
		{ name: 'Grimacing Face', text: '😬'},
		{ name: 'See-No-Evil Monkey', text: '🙈'},
		{ name: 'Thumbs Up', text: '👍'},
		{ name: 'Thumbs Down', text: '👎'},
		{ name: 'flexed biceps', text: '💪'},
		{ name: 'White Heavy Check Mark', text: '✅'},
		{ name: 'Cross Mark', text: '❌'},
		{ name: 'Heavy Plus Sign', text: '➕'},
		{ name: 'Heavy Minus Sign', text: '➖'},
		{ name: 'Question Mark', text: '❓'},
		{ name: 'Exclamation Mark', text: '❗'},
		{ name: 'Light Bulb', text: '💡'},
		{ name: 'Wrench', text: '🔧'},
		{ name: 'Star', text: '⭐'},
		{ name: 'Warning', text: '⚠️'}
	],
	table: {
		contentToolbar: [
			'tableColumn',
			'tableRow',
			'mergeTableCells'
		]
	},
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: 'de'
};

InlineEditor.builtinPlugins = inlinePlugins;
InlineEditor.defaultConfig = inlineConfig;

export default {
    ClassicEditor, InlineEditor
};